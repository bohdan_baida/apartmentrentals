﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroserviceClient
{
    public class MicroserviceClient : IMicroserviceClient
    {
        private IBus bus;
        public MicroserviceClient(IBus bus)
        {
            this.bus = bus;
        }

        public void Publish<TMessage>(TMessage message)
            where TMessage : class, new()
        {
            bus.Publish(message);
        }
        public async Task PublishAsync<TMessage>(TMessage message)
            where TMessage : class, new()
        {
            await bus.PublishAsync(message);
        }
        public TResponse Request<TRequest, TResponse>(TRequest message)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            return bus.Request<TRequest, TResponse>(message);
        }
        public async Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            return await bus.RequestAsync<TRequest, TResponse>(message);
        }
    }
}
