﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroserviceClient
{
    public interface IMicroserviceClient
    {
        void Publish<TMessage>(TMessage message)
                    where TMessage : class, new();
        Task PublishAsync<TMessage>(TMessage message)
          where TMessage : class, new();

        TResponse Request<TRequest, TResponse>(TRequest message)
           where TRequest : class, new()
           where TResponse : class, new();

        Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message)
          where TRequest : class, new()
          where TResponse : class, new();
    }
}
