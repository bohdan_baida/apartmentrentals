﻿using EasyNetQ;
using MicroserviceClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseMicroservice
{
    public class BaseMicroservice : IMicroservice, IMicroserviceClient
    {
        private IBus bus;
        private IMicroserviceClient microserviceClient;

        private readonly string subscriptionId;
        public BaseMicroservice()
        {
            var config = $"host={RabbitMqConfig.Host};" +
                $"virtualHost={RabbitMqConfig.VirtualHost};" +
                $"username={RabbitMqConfig.Username};" +
                $"password={RabbitMqConfig.Password}";
            bus = RabbitHutch.CreateBus(config);
            microserviceClient = new MicroserviceClient.MicroserviceClient(bus);

            subscriptionId = Guid.NewGuid().ToString();
        }

        public void Subscribe<T>(Action<T> onMessage)
            where T : class, new()
        {
            bus.Subscribe<T>(subscriptionId, onMessage);
        }
        public void SubscribeAsync<T>(Func<T, Task> onMessage)
            where T : class, new()
        {
            bus.SubscribeAsync<T>(subscriptionId, onMessage);
        }
        public void Respond<TRequest, TResponse>(Func<TRequest, TResponse> onMessage)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            bus.Respond<TRequest, TResponse>(onMessage);
        }
        public void RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> onMessage)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            bus.RespondAsync<TRequest, TResponse>(onMessage);
        }

        public void Publish<TMessage>(TMessage message) where TMessage : class, new()
        {
            microserviceClient.Publish(message);
        }
        public async Task PublishAsync<TMessage>(TMessage message) where TMessage : class, new()
        {
            await microserviceClient.PublishAsync(message);
        }
        public TResponse Request<TRequest, TResponse>(TRequest message)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            return microserviceClient.Request<TRequest, TResponse>(message);
        }
        public async Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message)
            where TRequest : class, new()
            where TResponse : class, new()
        {
            return await microserviceClient.RequestAsync<TRequest, TResponse>(message);
        }
    }
}
