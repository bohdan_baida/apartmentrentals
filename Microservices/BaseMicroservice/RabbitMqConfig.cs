﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseMicroservice
{
    public static class RabbitMqConfig
    {
        public static readonly string Host;
        public static readonly string VirtualHost;
        public static readonly string Username;
        public static readonly string Password;

        static RabbitMqConfig()
        {
            Host = ConfigurationManager.AppSettings.Get("rabbit_mq_host");
            VirtualHost = ConfigurationManager.AppSettings.Get("rabbit_mq_virtual_host");
            Username = ConfigurationManager.AppSettings.Get("rabbit_mq_username");
            Password = ConfigurationManager.AppSettings.Get("rabbit_mq_password");
        }
    }
}
