﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseMicroservice
{
    public interface IMicroservice
    {
        void Subscribe<T>(Action<T> onMessage)
            where T : class, new();
        void SubscribeAsync<T>(Func<T, Task> onMessage)
            where T : class, new();
        void Respond<TRequest, TResponse>(Func<TRequest, TResponse> onMessage)
            where TRequest : class, new()
            where TResponse : class, new();
        void RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> onMessage)
            where TRequest : class, new()
            where TResponse : class, new();
    }
}
